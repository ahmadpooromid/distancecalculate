﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class CalculationHistory : BaseEntity
    {
        public string Distance { get; set; }
        public int UserId { get; set; }

        public User User { get; set; }
    }
}