﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyApi.Models;
using WebFramework.Api;
using WebFramework.Filters;

namespace MyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiResultFilter]
    [ApiController]
    public class DistanceCalculationController : Controller
    {
        private readonly IDistanceCalQueries _distanceCalQueries;
        public DistanceCalculationController(IDistanceCalQueries distanceCalQueries)
        {
            _distanceCalQueries = distanceCalQueries;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<string>> Get(DistanceDto distanceDto)
        {
            //float OriginLatitude = 28.7041f;
            //float OriginLongitude = 77.1025f;
            //float DestinationLatitude = 26.9124f;
            //float DestinationLongitude = 75.7873f;

            var result = await _distanceCalQueries.CalculateDistanceBetweenTwo(distanceDto.OriginLatitude, distanceDto.OriginLongitude, distanceDto.DestinationLatitude, distanceDto.DestinationLongitude);

            return result.FirstOrDefault().ToString();
        }
    }
}