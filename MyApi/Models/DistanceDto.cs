﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyApi.Models
{
    public class DistanceDto
    {
        public float OriginLatitude { get; set; }
        public float OriginLongitude { get; set; }
        public float DestinationLatitude { get; set; }
        public float DestinationLongitude { get; set; }
    }
}
