﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Contracts
{
    public interface IDistanceCalQueries
    {
        Task<List<string>> CalculateDistanceBetweenTwo(float OriginLatitude, float OriginLongitude,float DestinationLatitude, float DestinationLongitude);
    }
}