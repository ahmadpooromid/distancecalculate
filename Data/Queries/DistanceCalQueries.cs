﻿using Dapper;
using Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Data.Queries
{
    public class DistanceCalQueries : IDistanceCalQueries
    {
        private readonly string _connectionString;
        public DistanceCalQueries(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<List<string>> CalculateDistanceBetweenTwo(float OriginLatitude, float OriginLongitude, float DestinationLatitude, float DestinationLongitude)
        {
            //var userId = ClaimTypes.NameIdentifier;
            //var userName = ClaimTypes.Name;

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return (await connection.QueryAsync<string>
                    (@"DECLARE @Tab AS TABLE ( Id int, Latitude float, Longitude float );  
                        INSERT INTO @Tab ( Id, Latitude, Longitude ) VALUES ( 1, "+ OriginLatitude + @", "+ OriginLongitude + @" )   
                        INSERT INTO @Tab ( Id, Latitude, Longitude ) VALUES ( 2,"+ DestinationLatitude + @", "+ DestinationLongitude + @" )  
                        DECLARE @Point1 geography;  
                        DECLARE @Point2 geography;  
                        set @Point1=(SELECT geography::Point(t.Latitude,t.Longitude,4326) FROM @Tab t WHERE t.Id=1)  
                        set @Point2=(SELECT geography::Point(t.Latitude,t.Longitude,4326) FROM @Tab t WHERE t.Id=2)  
                        --Calcultae Distance---  
                        SELECT @Point1.STDistance(@Point2) AS Distance_in_meters")).ToList();

                //insert into DistanceCalculateDB.dbo.CalculationHistories
                //        values("+ userName + @", "+ userId + ")")).ToList();
            }
        }
    }
}